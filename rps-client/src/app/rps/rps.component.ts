import {Component, OnInit} from '@angular/core'
import {RpsService} from './rps.service'
import {GamePlayer} from './game-player'

@Component({
  selector: 'app-rps',
  templateUrl: './rps.component.html',
  styleUrls: ['./rps.component.css']
})
export class RpsComponent implements OnInit {
  gamePlayer1 = new GamePlayer('', '')
  gamePlayer2 = new GamePlayer('', '')

  constructor(private rpsService: RpsService) {
  }

  ngOnInit() {
  }
}
