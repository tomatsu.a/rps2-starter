package rpsserver;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/rps")
public class RpsController {
    private RpsService rpsService;

    public RpsController(RpsService rpsService) {
        this.rpsService = rpsService;
    }
}
